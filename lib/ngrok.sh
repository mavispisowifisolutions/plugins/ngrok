#!/bin/bash
set -fb
readonly DIR=$(cd "$(dirname "$0")" ; pwd)

cd "${DIR}"
echo "authtoken: $1
tunnels:
  portal:
    addr: 80
    bind_tls: true
    proto: http

  ssh:
    addr: 22
    proto: tcp
" > ./ngrok.yml

./ngrok start --all --config ./ngrok.yml
exit 0
