
var path                        = require('path');
var ngrok                       = path.join(__dirname, 'lib', 'ngrok.sh');
var {spawn, execSync}           = require('child_process');
var child;
//register_options = { 'ngrok-authtoken1': '',}

form_handler = function( ){
  if ( posted['ngrok-authtoken1'] ) {
    return set_option('ngrok-authtoken1', posted['ngrok-authtoken1']).then(d=>{ d.restart = false; return d; });
  } else if ( posted['ngrok'] == 'start' ) {
    try { execSync("kill $(pgrep 'ngrok.sh')"); } catch (e) { };
    return get_option( 'ngrok-authtoken1' )
      .then(authtoken=>{
        try { execSync(ngrok + " " + authtoken + ' > /dev/null 2>&1 &'); } catch (e) { console.log(e)};
        return authtoken;
      })
      .then(d=> set_option('ngrok-start', 'yes') )
      .then(d=>{ d.restart = false; return d; });
  } else if ( posted['ngrok'] == 'stop' ) {
    try {
      console.log('stopping remote')
      execSync("kill $(pgrep 'ngrok')");
    } catch (e) {}
    return set_option('ngrok-start', 'no').then(d=>{ d.restart = false; return d; });
  } else {
    return false;
  }
}

data_store = function( next ){
  var data = {};
  Promise.all([
    get_option( 'ngrok-authtoken1' )
      .then(authtoken=>{ data.ngrok_authtoken = authtoken; return authtoken;}),
    get_option( 'ngrok-start' )
      .then(start=>{ data.start = start; return start;}),
  ])
  .then(result=>{ next(data) })
}
