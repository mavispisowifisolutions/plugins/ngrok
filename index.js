"use strict"
/*
* Plugin Name: Piso Wifi Online Admin.
* Author: Reigel Gallarde
* version: 1.2.1
* Description: Add ngrok support. Tunnel to Mavis Piso Wifi admin page. This will enable remote control.
*/
var path                        = require('path');
var ngrok                       = path.join(__dirname, 'lib', 'ngrok');
var ngrok_sh                    = path.join(__dirname, 'lib', 'ngrok.sh');
var {spawn, execSync}           = require('child_process');
var child;
console.log('ngrok running');
try { execSync("chmod +x " + ngrok ); } catch (e) {}
try { execSync("chmod +x " + ngrok_sh ); } catch (e) {}
get_option('ngrok-authtoken1').then(authtoken => {
  if ( authtoken ) {
    get_option('ngrok-start').then(d => { 
      if ( d && ( d == 'no' ) ) {
        try { execSync("kill $(pgrep 'ngrok')"); } catch (e) {}
      } else if ( d == 'yes' ) {
        try { execSync("kill $(pgrep 'ngrok.sh')"); } catch (e) {}
        try { execSync(ngrok_sh + " " + authtoken + ' > /dev/null 2>&1 &'); } catch (e) {}
      }
    })
  }
});

